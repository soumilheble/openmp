********** Introduction to OpenMP - Tim Mattson (Intel) **********
** https://computing.llnl.gov/tutorials/openMP/exercise.html

****** Introduction to OpenMP: 01 Introduction

****** Introduction to OpenMP: 02 part 1 Module 1

****** Introduction to OpenMP: 02 part 2 Module 1
** Concurrency
** Parallelism
** Expose concurrency in a problem then map them to processes and execute them at the same time.
** Concurrent Application
** Parallel Application
** Finding Concurrency in Problems
   * You have a block of work and a block of data that you need to keep a track of.
   * Divide your work into concurrent parts and add little more data to keep track of ordering or other stuff etc.
   * Organize concurrency to an algorithm and figure out the parallelism and map it into blocks that can run in parallel.
   * Then select the parallel programming language you want to implement the algorithm in.

****** Introduction to OpenMP: 03 Module 2

****** Introduction to OpenMP: 04 Discussion 1
** Challenge as a programmer is that every way you interleave the threads your process must give a correct result.